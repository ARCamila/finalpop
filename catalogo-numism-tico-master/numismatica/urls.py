"""numismatica URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from catalogo.views import index
from catalogo import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('numismatica/', index),
    path('numismatica/agregar/', views.ElementoCreateView.as_view(), name='elemento_add'),
    path('numismatica/listado/', views.ElementoListView.as_view(), name='elemento_changelist'),
    path('<int:pk>/', views.ElementoUpdateView.as_view(), name='elemento_change'),
    path('async/cargar-tipos-moneda/', views.cargar_tipo_moneda, name='async_cargar_tipos_moneda'),
]