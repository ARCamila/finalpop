from django.contrib import admin

# Register your models here.
from catalogo.models import *

admin.site.register(Pais)
admin.site.register(Elemento)
admin.site.register(TipoMoneda)